/*
 * +AMDG
 */
/*
 * This document was begun on 1X July 1202, the IX Sunday
 * after Pentecost, and it is humbly dedicated to the Sacred
 * Heart of Jesus, for His mercy.
 */

#define SUCCESS 0
#define OPT_REQ_ARG 1
#define UNREC_OPT 2
#define UNREC_LANG 3
#define UNREC_BOOKLIST 4
#define UNREC_BOOK 5
#define BAD_REGEX 6
#define INSUFF_MEM 7
#define BAD_CITE 8
#define BAD_SRC_LIST 9
#define BAD_DATA_DIR 10
