/*
 * +AMDG
 */
/*
 * This document was begun on 1X July 1202, the IX Sunday
 * after Pentecost, and it is humbly dedicated to the Sacred
 * Heart of Jesus, for His mercy.
 */

#define _XOPEN_SOURCE 700
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<getopt.h>
#include<ctype.h>
#include<string.h>
#include<dirent.h>
#include<regex.h>
#include<sys/types.h>
#include"bestline.h"
#include"errcodes.h"
#include"data_structs.h"
#include"utility.h"

#define NUMVERSES 36100

int num_chars_label(char *s);
int list_dir(char *s);
int read_file(char *s, char *st);
void book_name(char *s, char *t);
int pick_booklist(char *s);
void cleanup();
int build_verse(char *s, char *t, char *st, char *book,
		int chap, int chend, int verse, int vend);
int parse_query(char *st,char *book, int *chap, int *chend,
		int *verse, int *vend);
int clean_book(char *s,char *bookname);
int get_verse(char *bookname, char *book, int vchap, int chap,
		int chend, int vverse, int verse, int vend);
int search_verse(char *s);
void *xmalloc(int size);
void denumber(char *s);
void parse_opt(char *s);
int pop_langlist(void);

char *verses[NUMVERSES];
int countverses = 0;
char printverse = 0;
char printbook = 0;
int numprint = 0;
char language[MAX_ABB_LEN];
char booklist[220];
int casesense = 0; /* set to 1 for case insensitivity */

struct lang *langlist;
int numlist = 0;

int main(int argc, char *argv[])
{
	char c;
	char *line;
	int flag = 0;

	langlist = malloc(sizeof(struct lang));
	if ((flag = pop_langlist()) != 0) {
		free(langlist);
		exit(flag);
	}
	strcpy(language,"cvulg");
	strcpy(booklist,"1Cor1Jo1Mcc1Par1Ptr1Rg1Thes1Tim2Cor2Jo"
			"2Mcc2Par2Ptr2Rg2Thes2Tim3Jo3Rg4RgAbdActAggAmApcBar"
			"ColCtDnDtEclEphEsrEstExEzGalGnHabHbrIsJacJdcJdtJob"
			"JoelJoJonJosJrJudLamLcLvMalMchMcMtNahNehNmOsPhlmPhlp"
			"PrPsRomRtSapSirSophTitTobZach");
	opterr = 0;
	while ((c = getopt(argc,argv,"VBinsl:b:")) != -1) {
		switch (c) {
		case 'V':
			printf("cathbib v1.0\n");
			printf("Copyright (C) 1202 (2018) by Donald P.  Goodman III\n");
			printf("License GPLv3+:  GNU GPL Version 3 or later "
				"<http://gnu.org/licenses/gpl.html>\n");
			printf("This is free software; you are free to change "
				"and redistribute it.  There is NO WARRANTY, "
				"to the extent permitted by law.\n");
			exit(SUCCESS);
			break;
		case 's':
			list_src();
			exit(SUCCESS);
			break;
		case 'i':
			casesense = REG_ICASE;
			break;
		case 'l':
			if (lang_is_inlist(optarg) < 0) {
				fprintf(stderr,"cathbib:  error:  requested "
						"version \"%s\" is not known; version "
						"set to default \"cvulg\"\n",optarg);
			} else {
				strncpy(language,optarg,MAX_ABB_LEN);
			}
			break;
		case 'n':
			printverse = 1;
			break;
		case 'B':
			printbook = 1;
			break;
		case 'b':
			if ((flag = pick_booklist(optarg)) != 0) {
				exit(UNREC_BOOKLIST);
			}
			break;
		case '?':
			if ((optopt == 'l') || (optopt == 'b')) {
				fprintf(stderr,"cathbib:  option \"%c\" requires "
					"an argument\n",optopt);
				exit(OPT_REQ_ARG);
			}
			fprintf(stderr,"cathbib:  unrecognized option "
				"\"%c\"\n",optopt);
			exit(UNREC_OPT);
			break;
		}
	}
	if (argv[optind] != NULL) {
		if ((flag = list_dir(argv[optind])) != 0) {
			fprintf(stderr,"cathbib:  malformed query string\n");
			cleanup();
			exit(BAD_CITE);
		}
	} else {
		while ((line = bestlineWithHistory("cathbib> ","cathbib")) != NULL) {
			front_chomp(line);
			if (!strcmp(line,"exit") || !strcmp(line,"quit")) {
				bestlineFree(line);
				break;
			} else if (line[0] == '-') {
				if (strlen(line) >= 2) {
					parse_opt(line+1);
				} else {
					bestlineFree(line);
					continue;
				}
				bestlineFree(line);
			} else if (strlen(line) == 0) {
				bestlineFree(line);
				continue;
			} else {
				if ((flag = list_dir(line)) != 0) {
					bestlineFree(line);
					continue;
				} else {
					bestlineFree(line);
				}
			}
			cleanup();
		}
	}
	cleanup();
	return SUCCESS;
}

int pop_langlist(void)
{
	FILE *fp;
	char *confname = "/.config/cathbib/sources";
	char *filename;
	char *line = NULL;
	size_t len = 0;
	ssize_t nread;
	char *token;
	int j;
	struct lang *tmp;

	filename = malloc(strlen(getenv("HOME")) + strlen(confname) + 2);
	strcpy(filename,getenv("HOME"));
	strcat(filename,confname);
	if ((fp = fopen(filename,"r")) == NULL) {
		fprintf(stderr,"cathbib:  error:  couldn't get "
				"list of files from \"%s\"; quitting...\n",filename);
		free(filename);
		return BAD_SRC_LIST;
	}
	while ((nread = getline(&line,&len,fp)) != -1) {
		if ((tmp = realloc(langlist,sizeof(struct lang) *
						(numlist+1))) == NULL) {
			fprintf(stderr,"cathbib:  error:  insufficient memory\n");
			exit(INSUFF_MEM);
		} else {
			langlist = tmp;
		}
		j = 0;
		chomp(line);
		token = strtok(line,"\t");
		while (token != NULL) {
			if (j == 0) {
				strncpy(langlist[numlist].a,token,MAX_ABB_LEN);
			} if (j == 1) {
				strncpy(langlist[numlist].n,token,MAX_VERS_LEN);
			} if (j == 2) {
				strncpy(langlist[numlist].l,token,MAX_LANG_LEN);
			} if (j == 3) {
				strncpy(langlist[numlist].w,token,MAX_SRC_LEN);
			}
			++j;
			token = strtok(NULL,"\t");
		}
		++numlist;
	}
	free(line);
	free(filename);
	return 0;
}

void cleanup()
{
	int i;

	for (i = 0; i < countverses; ++i) {
		free(verses[i]);
	}
	countverses = 0;
}

int pick_booklist(char *s)
{
	int flag = 0;
	char buf[144];

	if (strstr(s,"moses") || strstr(s,"torah")) {
		strcpy(booklist,"GnExLvNmDt");
	} else if (strstr(s,"gospel") || strstr(s,"gosp")) {
		strcpy(booklist,"MtMcLcJo");
	} else if (strstr(s,"major")) {
		strcpy(booklist,"IsJrLamBarEzDn");
	} else if (strstr(s,"minor")) {
		strcpy(booklist,"OsJoelAmAbdJonMchNahHabSophAggZachMal");
	} else if (strstr(s,"prophets")) {
		strcpy(booklist,"IsJrLamBarEzDnOsJoelAmAbdJonMchNah"
				"HabSophAggZachMal");
	} else if (strstr(s,"peter")) {
		strcpy(booklist,"1Ptr2Ptr");
	} else if (strstr(s,"john")) {
		strcpy(booklist,"Jo1Jo2Jo3JoApc");
	} else if (strstr(s,"paul")) {
		strcpy(booklist,"Rom1Cor2CorGalEphPhlpCol1Thes2Thes"
				"1Tim2TimTitPhlmHbr");
	} else if (strstr(s,"old")) {
		strcpy(booklist,"GnExLvNmDtJosJdcRt1Rg2Rg3Rg4Rg1Par2Par"
				"EsrNehTobJudEstJobPsPrEclCtSapSirIsJrLamBarEzDnOs"
				"JoelAmAbdJonMchNahHabSophAggZachMal1Mcc2Mcc");
	} else if (strstr(s,"new")) {
		strcpy(booklist,"MtMcLcJoActRom1Cor2CorGalEphPhlpCol"
				"1Thes2Thes1Tim2TimTitPhlmHbrJac1Ptr2Ptr"
				"1Jo2Jo3JoJudApc");
	} else if (strstr(s,"cath") || strstr(s,"deutero") || strstr(s,"catholic")) {
		strcpy(booklist,"TobJudSapSirBar1Mcc2Mcc");
	} else if (strstr(s,"all")) {
		strcpy(booklist,"1Cor1Jo1Mcc1Par1Ptr1Rg1Thes1Tim2Cor2Jo"
				"2Mcc2Par2Ptr2Rg2Thes2Tim3Jo3Rg4RgAbdActAggAmApcBar"
				"ColCtDnDtEclEphEsrEstExEzGalGnHabHbrIsJacJdcJdtJob"
				"JoelJoJonJosJrJudLamLcLvMalMchMcMtNahNehNmOsPhlmPhlp"
				"PrPsRomRtSapSirSophTitTobZach");
	} else {
		if ((flag = clean_book(s,buf)) != 0) {
		} else {
			strcpy(booklist,buf);
		}
	}
	return flag;
}

void parse_opt(char *s)
{
	int i; int j = 0;
	char *t;

	t = xmalloc(strlen(s) + 1);
	for (i = 0; s[i] != '\0'; ++i) {
		if (!isspace(s[i]))
			t[j++] = s[i];
	}
	t[j] = '\0';
	if (s[0] == 'l') {
		if (lang_is_inlist(t+1) < 0) {
			fprintf(stderr,"cathbib:  error:  requested "
					"version \"%s\" is not known; version "
					"set to default \"cvulg\"\n",t);
		} else {
			strncpy(language,t+1,MAX_ABB_LEN);
		}
	} else if (s[0] == 's') {
		list_src();
	} else if (s[0] == 'b') {
		pick_booklist(t+1);
	} else {
		fprintf(stderr,"cathbib:  error:  option \"%c\" "
				"is not recognized\n",s[0]);
	}
	free(t);
}

int list_dir(char *s)
{
	DIR *d;
	struct dirent *ent;
	char bookname[6];
	int flag = 0;
	char *p;

	numprint = 0;
	if ((d = opendir(DATA_DIR)) != NULL) {
		while ((ent = readdir(d)) != NULL) {
			if ((p = strstr(ent->d_name,language)) != NULL) {
				if (*(p+strlen(language)) != '\0')
					continue; /* to ensure that language matches exactly */
				book_name(ent->d_name,bookname);
				if (strstr(booklist,bookname)) {
					if ((flag = read_file(ent->d_name,s)) != 0) {
						closedir(d);
						return -1;
					}
				}
			}
		}
	} else {
		fprintf(stderr,"cathbib:  error:  could not find text "
				"directory \"%s\"; quitting...\n",DATA_DIR);
		exit(BAD_DATA_DIR);
	}
	if (s[0] == '/') {
		printf("*** (%d) results for \"%s\" ***\n", numprint, s);
	}
	closedir(d);
	return 0;
}

int read_file(char *s, char *st)
{
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	ssize_t nread;
	char *t;
	int length;
	char buf[144];
	char book[8];
	int chap = 0; int chend = 0; int verse = 0; int vend = 0;
	int flag = 0;

	length = strlen(s) + 1;
	t = xmalloc((sizeof(DATA_DIR) + length + 1) * sizeof(char));
	strcpy(t,DATA_DIR);
	strcat(t,s);
	if ((fp = fopen(t,"r")) == NULL) {
		fprintf(stderr,"cathbib:  error:  could not open file "
				"\"%s\"; skipping...\n",t);
		return 0;
	}
	if (st[0] != '/') {
		if ((flag = parse_query(st,buf,&chap,&chend,&verse,&vend)) != 0) {
			free(t);
			fclose(fp);
			return -1;
		}
		if ((flag = clean_book(buf,book)) != 0) {
			free(t);
			fclose(fp);
			return -1;
		}
	}
	while ((nread = getline(&line, &len, fp)) != -1) {
		if ((flag = build_verse(line,s,st,book,chap,chend,verse,vend)) == 2) {
			fclose(fp);
			free(line);
			free(t);
			return -1;
		}
	}
	free(line);
	free(t);
	fclose(fp);
	return 0;
}

int build_verse(char *s, char *t, char *st, char *book,
		int chap, int chend, int verse, int vend)
{
	int len;
	char bookname[8];
	int vchap = 0; int vverse = 0;
	char *p;
	int flag;

	sscanf(s,"%d:%d",&vchap,&vverse);
	book_name(t,bookname);
	len = strlen(s);
	len = len + 5 + 3 + 2 + 1;
	verses[countverses] = xmalloc(len * sizeof(char));
	strcpy(verses[countverses],s);
	if (st[0] != '/') {
		if (get_verse(bookname,book,vchap,chap,chend,vverse,verse,vend) == 1) {
			if (printbook == 1)
				printf("%s ",bookname);
			if (printverse == 0)
				p = verses[countverses];
			else
				p = verses[countverses] + num_chars_label(verses[countverses]);
			printf("%s",p);
		}
	} else {
		if ((flag = search_verse(st)) == 1) {
			printf("%s %s",bookname,verses[countverses]);
			++numprint;
		} else if (flag == 2) {
			return 2;
		}
	}
	countverses++;
	return 0;
}

int num_chars_label(char *s)
{
	int i = 0;

	while (isdigit(s[i]) || s[i] == ':' || isspace(s[i]))
		++i;
	return i;
}

/* return 1 if should be printed, 0 if not, 2 if error */
int search_verse(char *s)
{
	regex_t regex;
	int reti;
	char buf[100];

	if ((reti = regcomp(&regex,(s+1),casesense)) != 0) {
		regerror(reti,&regex,buf,99);
		fprintf(stderr,"cathbib:  regular expression \"%s\" "
				"is badly formed: %s; aborting\n",(s+1),buf);
		regfree(&regex);
		return 2;
	}
	if ((reti = regexec(&regex,verses[countverses],0,NULL,0)) == 0) {
		regfree(&regex);
		return 1;
	}
	regfree(&regex);
	return 0;
}

/* return 1 if should be printed, 0 if not */
int get_verse(char *bookname, char *book, int vchap, int chap,
		int chend, int vverse, int verse, int vend)
{
	if (vend == 0)
		vend = verse;
	if (!strcmp(book,bookname)) {
		if (chap == chend) {
			if ((vchap == chap) && (vverse >= verse) && (vverse <= vend)) {
				return 1;
			}
		} else {
			if (vchap >= chap) {
				if (vchap == chend) {
					if (vverse <= vend) {
						return 1;
					}
				} else if (vchap == chap) {
					if (vverse >= verse) {
						return 1;
					}
				} else if (vchap <= chend) {
					return 1;
				} else {
					return 0;
				}
			}
		}
	}
	return 0;
}

int parse_query(char *st,char *book, int *chap, int *chend,
		int *verse, int *vend)
{
	char *pats[] = {
		"\\([0-9]*[A-Za-z][A-Za-z]*\\)\\([0-9][0-9]*\\):\\([0-9][0-9]*\\)-\\([0-9][0-9]*\\):\\([0-9][0-9]*\\)",
		"\\([0-9]*[A-Za-z][A-Za-z]*\\)\\([0-9][0-9]*\\):\\([0-9][0-9]*\\)-\\([0-9][0-9]*\\)",
		"\\([0-9]*[A-Za-z][A-Za-z]*\\)\\([0-9][0-9]*\\):\\([0-9][0-9]*\\)",
		"\\([0-9]*[A-Za-z][A-Za-z]*\\)\\([0-9][0-9]*\\)-\\([0-9][0-9]*\\)",
		"\\([0-9]*[A-Za-z][A-Za-z]*\\)\\([0-9][0-9]*\\)"
	};

	regmatch_t pmatch[6];
	regex_t regex;
	int reti;
	char buf[100];
	int i;
	int j = 0;
	int result;
	char *tmp;

	tmp = xmalloc((strlen(st) + 1) * sizeof(char));
	for (i = 0; st[i] != '\0'; ++i) {
		if (!isspace(st[i]))
			tmp[j++] = st[i];
	}
	tmp[j] = '\0';
	for (i = 0; i < 5; ++i) {
		if ((reti = regcomp(&regex,pats[i],0)) != 0) {
			regerror(reti,&regex,buf,99);
			fprintf(stderr,"cathbib:  malformed regex\n");
			exit(BAD_REGEX);
		}
		result = regexec(&regex,tmp,6,pmatch,0);
		regfree(&regex);
		if (result == 0) {
			switch (i) {
			case 0:
				sprintf(book,"%*s",(int) (pmatch[1].rm_eo -
						pmatch[1].rm_so),tmp+pmatch[1].rm_so);
				denumber(book);
				sprintf(buf,"%*s",(int) (pmatch[2].rm_eo -
						pmatch[2].rm_so),tmp+pmatch[2].rm_so);
				*chap = atoi(buf);
				sprintf(buf,"%*s",(int) (pmatch[3].rm_eo -
						pmatch[3].rm_so),tmp+pmatch[3].rm_so);
				*verse = atoi(buf);
				sprintf(buf,"%*s",(int) (pmatch[4].rm_eo -
						pmatch[4].rm_so),tmp+pmatch[4].rm_so);
				*chend = atoi(buf);
				sprintf(buf,"%*s",(int) (pmatch[5].rm_eo -
						pmatch[5].rm_so),tmp+pmatch[5].rm_so);
				*vend = atoi(buf);
				if (*chend < *chap) {
					fprintf(stderr,"cathbib:  invalid chapter range "
							"of %d - %d\n",*chap,*chend);
					free(tmp);
					return -1;
				}
				free(tmp);
				return 0;
				break;
			case 4:
				sprintf(book,"%*s",(int) (pmatch[1].rm_eo -
						pmatch[1].rm_so),tmp+pmatch[1].rm_so);
				denumber(book);
				sprintf(buf,"%*s",(int) (pmatch[2].rm_eo -
						pmatch[2].rm_so),tmp+pmatch[2].rm_so);
				*chap = atoi(buf);
				*chend = atoi(buf);
				*verse = 0;
				*vend = 9999;
				free(tmp);
				return 0;
				break;
			case 3:
				sprintf(book,"%*s",(int) (pmatch[1].rm_eo -
						pmatch[1].rm_so),tmp+pmatch[1].rm_so);
				denumber(book);
				sprintf(buf,"%*s",(int) (pmatch[2].rm_eo -
						pmatch[2].rm_so),tmp+pmatch[2].rm_so);
				*chap = atoi(buf);
				sprintf(buf,"%*s",(int) (pmatch[3].rm_eo -
						pmatch[3].rm_so),tmp+pmatch[3].rm_so);
				*chend = atoi(buf);
				*verse = 0;
				*vend = 9999;
				free(tmp);
				return 0;
			break;
			case 2:
				sprintf(book,"%*s",(int) (pmatch[1].rm_eo -
						pmatch[1].rm_so),tmp+pmatch[1].rm_so);
				denumber(book);
				sprintf(buf,"%*s",(int) (pmatch[2].rm_eo -
						pmatch[2].rm_so),tmp+pmatch[2].rm_so);
				*chap = atoi(buf);
				*chend = atoi(buf);
				sprintf(buf,"%*s",(int) (pmatch[3].rm_eo -
						pmatch[3].rm_so),tmp+pmatch[3].rm_so);
				*verse = atoi(buf);
				free(tmp);
				return 0;
			break;
			case 1:
				sprintf(book,"%*s",(int) (pmatch[1].rm_eo -
						pmatch[1].rm_so),tmp+pmatch[1].rm_so);
				denumber(book);
				sprintf(buf,"%*s",(int) (pmatch[2].rm_eo -
						pmatch[2].rm_so),tmp+pmatch[2].rm_so);
				*chap = atoi(buf);
				*chend = atoi(buf);
				sprintf(buf,"%*s",(int) (pmatch[3].rm_eo -
						pmatch[3].rm_so),tmp+pmatch[3].rm_so);
				*verse = atoi(buf);
				sprintf(buf,"%*s",(int) (pmatch[4].rm_eo -
						pmatch[4].rm_so),tmp+pmatch[4].rm_so);
				*vend = atoi(buf);
				if (*vend < *verse) {
					fprintf(stderr,"cathbib:  invalid verse range "
							"of %d - %d\n",*verse,*vend);
					free(tmp);
					return -1;
				}
				free(tmp);
				return 0;
				break;
			}
		}
	}
	fprintf(stderr,"cathbib:  error:  cannot parse "
			"query string \"%s\"; skipping...\n",st);
	free(tmp);
	return -1;
}

void book_name(char *s,char *t)
{
	int i;

	for (i = 0; s[i] != '.' && s[i] != '\0'; ++i)
		t[i] = s[i];
	t[i] = '\0';
}

int clean_book(char *s,char *bookname)
{
	int i;
	int j = 0;
	char *t;
	char new[18];
	struct syns {
		char canname[8];
		char *syno;
	} syns[] = {
		{"Gn", " gen gene genesis "},
		{"Ex", " exo exod exodus "},
		{"Lv", " lev leviticus levi "},
		{"Nm", " num number numbers "},
		{"Dt", " deu deut deuteronomy deuter "},
		{"Jos", " josh josue joshua "},
		{"Jdc", " judg judge judges "},
		{"Rt", " rut ruth "},
		{"1Rg", " 1sam 1samuel 1king 1kings "},
		{"2Rg", " 2sam 2samuel 2king 2kings "},
		{"3Rg", " 3king 3kings "},
		{"4Rg", " 4king 4kings "},
		{"1Par", " 1chr 1chron 1chronicles 1para 1paral 1paralip 1paralipomenon "},
		{"2Par", " 2chr 2chron 2chronicles 2para 2paral 2paralip 2paralipomenon "},
		{"Esr", " 1esd 1esdras ezra "},
		{"Neh", " 2esd 2esdras nehemiah "},
		{"Tob", " tobias tobiah tobit "},
		{"Jdt", " judith judi "},
		{"Est", " esther esth "},
		{"Ps", " psalm psalms "},
		{"Pr", " prov proverb proverbs "},
		{"Ecl", " ecclesiastes eccles "},
		{"Job", " job jb "},
		{"Ct", " cant canticle canticles song "},
		{"Sap", " ws wis wisd wisdom sap sapientia sapientiae "},
		{"Sir", " sirach ecclus ecclesiasticus eccli "},
		{"Is", " isaiah isaias isa isai "},
		{"Jr", " jer jeremiah jeremias jere jerem "},
		{"Lam", " lamen lamentations "},
		{"Bar", " baruch baru "},
		{"Ez", " ezechiel ezekiel ezeckiel ezek ezech eze "},
		{"Dn", " daniel dan "},
		{"Os", " osee hosea ose hose hos "},
		{"Joel", " joe "},
		{"Am", " amos amo "},
		{"Abd", " abdias obadiah oba "},
		{"Jon", " jonah jonas "},
		{"Mch", " micheas micah "},
		{"Nah", " nahum "},
		{"Hab", " habacuc habakkuk hab "},
		{"Soph", " sophonias sophoniah zephaniah zephanias zeph "},
		{"Agg", " aggeus haggai hag "},
		{"Zach", " zacharias zachariah zachar zech zechariah "},
		{"Mal", " malachias malachi "},
		{"1Mcc", " 1mac 1machabee 1maccabee 1machabees 1maccabees "},
		{"2Mcc", " 2mac 2machabee 2maccabee 2machabees 2maccabees "},
		{"Mt", " matt matthew "},
		{"Mc", " mark marc "},
		{"Lc", " luke luc luca lucas lk "},
		{"Jo", " john "},
		{"Act", " acta acts act "},
		{"Rom", " romans roman rm "},
		{"1Cor", " 1corin 1corinthian 1corinthians "},
		{"2Cor", " 2corin 2corinthian 2corinthians "},
		{"Gal", " galatian galatians galat "},
		{"Eph", " eph ephes ephesian ephesians "},
		{"Phlp", " philip philippian philippians "},
		{"Col", " colos colossian colossians "},
		{"1Thes", " 1thess 1thes 1thessa 1thessal 1thessalonian 1thessalonians "},
		{"2Thes", " 2thess 2thes 2thessa 2thessal 2thessalonian 2thessalonians "},
		{"1Tim", " 1tim 1timothy "},
		{"2Tim", " 2tim 2timothy "},
		{"Tit", " titus "},
		{"Phlm", " phile philem philemon "},
		{"Hbr", " heb hebrew hebrews "},
		{"Jac", " james jam jacob jacobus "},
		{"1Ptr", " 1pt 1peter "},
		{"2Ptr", " 2pt 2peter "},
		{"1Jo", " 1john "},
		{"2Jo", " 2john "},
		{"3Jo", " 3john "},
		{"Jud", " jude "},
		{"Apc", " rev revel revelation revelations apc apoc apocalypse "},
	};

	t = xmalloc((strlen(s) + 1 + 2) * sizeof(char));
	t[j++] = ' ';
	for (i = 0; s[i] != '\0'; ++i) {
		if (!isspace(s[i])) {
			t[j++] = tolower(s[i]);
		}
		if ((i > 0) && (isdigit(s[i+1]))) {
			break;
		}
	}
	t[j++] = ' ';
	t[j] = '\0';
	for (i = 0; i < 73; ++i) {
		strncpy(new,s,17);
		new[0] = toupper(new[0]);
		if (!strcmp(new,syns[i].canname)) {
			strcpy(bookname,new);
			free(t);
			return 0;
		} else if (strstr(syns[i].syno,t)) {
			strcpy(bookname,syns[i].canname);
			free(t);
			return 0;
		}
	}
	fprintf(stderr,"cathbib:  book or list name \"%s\" is not "
			"recognized\n",s);
	free(t);
	return UNREC_BOOK;
}

void *xmalloc(int size)
{
	void *ptr;
	ptr = (void *) malloc (size);
	if (ptr == NULL) {
		fprintf(stderr,"cathbib:  insufficient memory; aborting\n");
		exit(INSUFF_MEM);
	}
	return ptr;
}

void denumber(char *s)
{
	int i;
	
	for (i = 1; s[i] != '\0'; ++i) {
		if (isdigit(s[i])) {
			s[i] = '\0';
			break;
		}
		if (s[i] == ' ') {
			s[i] = '\0';
			break;
		}
	}
}
