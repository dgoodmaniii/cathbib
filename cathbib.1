." +AMDG
." Process with:
." groff -man -Tascii cathbib.1
.TH cathbib 1 "October 2011" Linux "User Manuals"
.SH NAME
cathbib \- Multilingual Bible search and citation tool
.SH SYNOPSIS
.B cathbib [-VinB] [-b \fIl language\fR] [-b \fIbooklist\fR] "[\fIquery\fR]"
.SH DESCRIPTION
.B cathbib
A multilingual Bible search and citation tool.  Comes with
the Clementine Vulgate (Latin), the Douay-Rheims (English),
and the Septuagint/Greek New Testament (Greek) as texts, but is
easily extensible to other versions and languages.  Very
general in application and simple to use.
.SH OPTIONS
.IP -V
Print version and copyright information and exit
successfully.
.IP -n
Results will be printed \fIwithout\fR chapter and verse
number.  By default, they are printed \fIwith\fR chapter and
verse number.
.IP -B
Results will be printed \fIwith\fR book abbreviation.  By
default, they are printed \fIwithout\fR the book
abbreviation.
.IP -i
Searches should be case-insensitive; that is, capital and
lowercase versions of the same letter are considered the same.
The (sane) default is that searches are case-sensitive; that is,
capital and lowercase versions of the same letter are
considered different.
.IP -l \fIlanguage\fR
The language you wish to search.  This is something of a
misnomer, as what this really checks is the extension of the
book files.  Therefore, one could have book files with the
extension "kjv" to search the King James Version in English,
even though the "dorh" extension applies to the Douay-Rheims
version.
.RS
.PP
\fBcathbib\fR comes with the texts for the Clementine
Vulgate (suffixed "cvulg"), which is the default; the
Douay-Rheims (suffixed "dorh"); and the Greek Septuagint
(suffixed "sept").  The Clementine Vulgate is
the definitive Catholic version; the Douay-Rheims is the
best currently existing English translation, at least which
is not subject to copyright; and the Septuagint is a Jewish
translation into Greek made sometime around the third
century B.C.  The Septuagint version does include the Greek
New Testament.
.PP
\fBcathbib\fR is easily extensible for other languages and
versions.  You simply need to have files for each book you
want searched suffixed appropriately; it does *not* need to
be the same number of books as the Catholic Bible.  So if
you have files suffixed "kjv"---Gn.kjv, Ex.kjv, and so
forth---you can search them by specifying \fI-l "kjv"\fR
just as you would the Douay-Rheims with \fI-l "dorh"\fR.
.RE
.IP -b \fIbooklist\fR
The booklist you'd like to search.  By default,
\fBcathbib\fR searches the entire Bible; however, you can
direct it to limit your results to only a subset.  Each individual book
can be a booklist; \fBcathbib\fR also offers several lists
of books which can be used.  Equivalencies are listed
together.
.RS
.IP "gospel, gosp"
Matthew, Mark, Luke, and John.
.IP "moses, torah"
Genesis, Exodus, Leviticus, Numbers, and Deuteronomy.  These
have traditionally been known as the "Books of Moses" or as
the "Torah."
.IP "major"
Isaiah, Jeremiah, Lamentations, Baruch, Ezekiel, and Daniel.
Traditionally, these are known as the "major prophets."
.IP "minor"
Osee (Hosea), Joel, Amos, Abdias (Obadiah), Jonas (Jonah),
Micah, Nahum, Habakkuk, Sophanias (Zephaniah), Aggai
(Haggai), Zacharias (Zechariah), Malachi.  Traditionally,
these are known as the "minor prophets."
.IP "prophets"
Both the major and the minor prophets.
.IP "peter"
The letters of St. Peter.
.IP "john"
The writings of St. John.  Specifically, the Gospel of John;
the first, second, and third letters of John; and the
Apocalypse (Revelations).
.IP "paul"
Romans, 1 Corinthians, 2 Corinthians, Galations, Ephesians,
Philippians, Colossians, 1 Thessalonians, 2 Thessalonians, 1
Timothy, 2 Timothy, Titus, Philemon, and Hebrews.
.IP "old"
The Old Testament, including the deuterocanonical books if
they are present.  That is, Genesis, Exodus, Leviticus,
Numbers, Deuteronomy, Joshuah, Judges, Ruth, 1 Kings (1
Samuel), 2 Kings (2 Samuel), 3 Kings, 4 Kings, 1
Paralipomenon (1 Chronicles), 2 Paralipomenon (2
Chronicles), Esdras (Ezra), Nehemiah, Tobit, Judith, Esther,
Job, Psalms, Proverbs, Ecclesiastes, Canticle of Canticles
(Song of Songs), Wisdom, Sirach, Isaiah, Jeremiah,
Lamentations, Baruch, Ezekiel, Daniel, Osee (Hosea), Joel,
Amos, Abdias (Obadiah), Jonah, Micah, Nahum, Habakkuk,
Sophanias (Zephaniah), Aggeus (Haggai), Zachariah
(Zechariah), Malachi, 1 Maccabees, and 2 Maccabees.
.IP "new"
The New Testament.  That is, Matthew, Mark, Luke, John,
Acts, Romans, 1 Corinthians, 2 Corinthians, Galations,
Ephesians, Philippians, Colossians, 1 Thessalonians, 2
Thessalonians, 1 Timothy, 2 Timothy, Titus, Philemon,
Hebrews, James, 1 Peter, 2 Peter, 1 John, 2 John, 3 John,
Judah, and the Apocalypse (Revelations).
.IP "deutero"
The "deuterocanonical" books; that is, those books which
Catholics and Orthodox accept as canonical but Protestants
do not.  Tobit; Judith; Wisdom; Sirach; Baruch; 1 Maccabees;
2 Maccabees.  \fBcathbib\fR makes no attempt to separate out
of Esther and Daniel the parts that Protestants do not
accept; however, a Protestant Bible text without those parts
would work just fine.
.IP "all"
The entire Bible.
.RE
.SH VERSE FORMATS
.PP
\fBcathbib\fR can recognize a variety of citation formats:
.IP "Book Chap"
.IP "Book Chap - Chap"
.IP "Book Chap : Verse"
.IP "Book Chap : Verse - Verse"
.IP "Book Chap : Verse - Chap : Verse"
.PP
Note that spaces can be added or not to these formats, as
wished; in other words, "Mt1:1-3" and "Mt   1 :   1 -  3"
are identical, as far as \fBcathbib\fR is concerned.
.PP
You cannot print across book boundaries; you'll have to
print the ranges across books manually.  E.g., you  can't
say "Mark 15:4-Luke2:3".
.SH ADDITIONAL VERSIONS AND LANGUAGES
.PP
Additional versions and languages can be added almost
trivially; there are only a few formatting restraints for
the texts.
.IP
Each book must have a unique extension; e.g., "kjv" or
"rsv".
.IP
Each book must have the canonical abbreviation; e.g.,
"Gn" for Genesis.
.IP 
Each book must be formatted with one verse per line,
beginning with the chapter, a colon, and the verse number.
.PP
This formatting can typically be accomplished pretty simply,
by script.  \fBcathbib\fR can handle an arbitrary number of
languages or versions of the Bible, provided they all have a
unique file extension.
.PP
\fBcathbib\fR looks for the versions it recognizes in a file
called \fIsources\fR, which is located in 
\fI~/.config/cathbib/sources\fR, and its lines are formatted
pretty fascistically in the following way:
.PP
.RS
abbv TAB translation TAB language TAB source
.RE
.PP
For example, the line for the Clementine Vulgate looks like
this (the line break shouldn't be there; it's just that
\fIman\fR doesn't deal well with long lines):
.PP
.RS
cvulg	Clementine Vulgate	Latin	https://vulsearch.sourceforge.net
.RE
.PP
As another example, if you wanted a line for the King James
version, it might look like this (with a source completely
made up):
.PP
.RS
kjv	King James	English	kingjamesversion.example.com
.RE
.PP
\fBcathbib\fR won't recognize any translation that isn't
listed in this \fIsources\fR file; but if it is listed here,
\fBcathbib\fR will be able to hunt it down in its DATA_DIR.
.PP
Further, if \fBcathbib\fR can't find at least three tabs in
the line, it will simply ignore it; if it can, it will parse
it, even if you don't have meaningful data in it.
.SH ERRORS
.IP 0
All went well and we exit successfully.  Yay!
.IP 1
An option that requires an argument doesn't have one.
.IP 2
You gave \fBcathbib\fR an argument that it doesn't
recognize.
.IP 3
The language you gave \fBcathbib\fR with the \fI-l\fR option
isn't one that \fBcathbib\fR recognizes.
.IP 4
The booklist you want \fBcathbib\fR to search isn't
recognized; see above for the booklists we have.
.IP 5
The book you want \fBcathbib\fR to look through isn't
recognized.
.IP 6
The regular expression you gave \fBcathbib\fR to search with
is malformed in some way; a message explaining it what way
should be printed with this, to stderr.
.IP 7
Your computer ran out of memory to give \fBcathbib\fR.  This
error is exceedingly unlikely, but when it happens,
\fBcathbib\fR aborts.
.IP 8
You've provided a citation for \fBcathbib\fR to fetch which
it can't parse.
.SH BUGS
None known at this time.
.SH AUTHOR
Donald P. Goodman III <dgoodmaniii at gmail dot com>
