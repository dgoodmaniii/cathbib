#!/bin/sh
# +AMDG

for file in *; do
	iconv -f CP1252 -t utf8 -o "$file.new" "$file"
	mv -f "$file.new" "$file"
done
