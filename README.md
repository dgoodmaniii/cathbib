NOTE:  I have recently become aware that integer overflows
can sometimes cause security issues.  I don't think that's a
problem with this program, but I don't have the expertise or
bandwidth to guarantee that.  So, as always, use at your own
risk.
