/*
 * +AMDG
 */
/*
 * This program was begun on 10 Jan 1207, a feria, and it is
 * humbly dedicated to the Blessed Virgin Mary, for her
 * prayers, and to the Infant Jesus, for His mercy.
 */

#define MAX_ABB_LEN 16
#define MAX_LANG_LEN 16
#define MAX_VERS_LEN 256
#define MAX_SRC_LEN 512

struct lang {
	char a[MAX_ABB_LEN+1];
	char n[MAX_VERS_LEN+1];
	char l[MAX_LANG_LEN+1];
	char w[MAX_SRC_LEN+1];
};
