# +AMDG
# This document was begun on 1X July 1202, the IX Sunday
# after Pentecost, and it is humbly dedicated to the Sacred
# Heart of Jesus, for His mercy.

.POSIX:

CC = gcc
CCFLAGS = -g -Wall -pedantic
DATA_DIR = "\"$(HOME)/.config/cathbib/texts/\""
OBJ = utility.o bestline.o
LIBS = bestline.h errcodes.h utility.h data_structs.h
PREFIX=/usr/local
BINFILE=cathbib
MANFILE=cathbib.1
SRCFILE=sources

all: cathbib

cathbib : main.c $(LIBS) $(OBJ)
	$(CC) $(CCFLAGS) -g -DDATA_DIR=$(DATA_DIR) -o cathbib main.c $(OBJ)

bestline.o : bestline.c bestline.h
	$(CC) $(CCFLAGS) -g -c bestline.c

utility.o : utility.h utility.c data_structs.h
	$(CC) $(CCFLAGS) -g -c utility.c

.PHONY: install
install:
	-mkdir -p $(HOME)/.config/cathbib/texts
	-cp -f $(DATA_DIR)/* $(HOME)/.config/cathbib/texts/
	$(CC) $(CCFLAGS) -g -DDATA_DIR="\"$(HOME)/.config/cathbib/texts/\"" -o cathbib main.c $(OBJ)
	-cp -f $(SRCFILE) $(HOME)/.config/cathbib/
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(PREFIX)/share/man/man1
	cp -f $(BINFILE) $(DESTDIR)$(PREFIX)/bin
	cp -f $(MANFILE) $(DESTDIR)$(PREFIX)/share/man/man1/

.PHONY: uninstall
uninstall :
	rm $(DESTDIR)$(PREFIX)/bin/$(BINFILE)
	rm $(DESTDIR)$(PREFIX)/share/man/man1/$(MANFILE)
	rm -Rf $(HOME)/.config/cathbib/

.PHONY: clean
clean :
	rm $(BINFILE) *.o
