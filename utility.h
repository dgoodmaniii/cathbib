/*
 * +AMDG
 */
/*
 * This program was begun on 10 Jan 1207, a feria, and it is
 * humbly dedicated to the Blessed Virgin Mary, for her
 * prayers, and to the Infant Jesus, for His mercy.
 */

void chomp(char *s);
int front_chomp(char *s);
int lang_is_inlist(char *s);
int list_src(void);
