/*
 * +AMDG
 */
/*
 * This program was begun on 10 Jan 1207, a feria, and it is
 * humbly dedicated to the Blessed Virgin Mary, for her
 * prayers, and to the Infant Jesus, for His mercy.
 */

#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include"data_structs.h"

extern struct lang *langlist;
extern int numlist;

void chomp(char *s)
{
	int i;

	for (i=strlen(s) - 1; i >= 0; --i) {
		if ((s[i] == '\n') || (s[i] == '\t') || (s[i] == ' '))
			s[i] = '\0';
		else
			break;
	}
}

int front_chomp(char *s)
{
	int i;
	for (i = 0; isspace(*(s+i)); ++i);
	memmove(s,s+i,strlen(s+i)+1);
	return 0;
}

/* return 0 if in list; -1 if not */
int lang_is_inlist(char *s)
{
	int i;

	for (i = 0; i < numlist; ++i) {
		if (strcmp(langlist[i].a,s) == 0) {
			return 0;
		}
	}
	return -1;
}

int list_src(void)
{
	int i;

	for (i = 0; i < numlist; ++i) {
		printf("%s\n",langlist[i].n);
		printf("\tLANGUAGE:\t%s\n",langlist[i].l);
		printf("\tABBREV.: \t%s\n",langlist[i].a);
		printf("\tSOURCE:  \t%s\n",langlist[i].w);
	}
	return 0;
}
